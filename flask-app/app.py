from flask import Flask, Response, render_template, request
import sys
from datetime import datetime

from flaskext.mysql import MySQL
from flask_accept import accept
import json


# Configure PORT
PORT = 5000 
if len(sys.argv) > 1:
    PORT = int(sys.argv[1])

# Create a flask APP object
app = Flask(__name__)


# Setup the DB CONFIGS in Flask Object
app.config["MYSQL_DATABASE_HOST"]= "localhost"
app.config["MYSQL_DATABASE_DB"]= "sakila"
app.config["MYSQL_DATABASE_USER"]= "root"
app.config["MYSQL_DATABASE_PASSWORD"]= "root"

# Create MYSQL Object
mysql = MySQL()
mysql.init_app(app)

conn = mysql.connect()


@app.route("/films", methods=["GET"])
@accept("text/html")
def getfilms():
    # Test , by do a SELECT * FROM film;
    limit = 20
    if request.args.get("limit"):
        limit = int(request.args.get("limit") )

    cursor = conn.cursor()
    cursor.execute("SELECT * FROM film LIMIT %s", [limit])
    resultset = cursor.fetchall()

    return render_template("films.html", movies = resultset)

@app.route("/films", methods=["GET"])
@getfilms.support("application/json")
def getfilms_json():
    limit = 20
    if request.args.get("limit"):
        limit = int(request.args.get("limit") )

    cursor = conn.cursor()
    cursor.execute("SELECT * FROM film LIMIT %s", [limit])
    resultset = cursor.fetchall()

    # resultset is tuple of tuple\
    tmp_list = []
    for record in resultset:
        movie_id, genre, desc, *tmp = record
        tmp_list.append( {
            "movie_id" : movie_id,
            "genre": genre,
            "desc": desc,
        } )

    return json.dumps(tmp_list, indent=4)

# Add a base route
@app.route("/", methods=['GET'])
def root():
    resp_data = "<h1> Current time = {} </h1>".format( str(datetime.now()) )
    return Response(response=resp_data, status=200, mimetype="text/html")

# Add a base route
@app.route("/time", methods=['GET'])
def time():
    # resp_data = "<h1> Current time = {} </h1>".format( str(datetime.now()) )
    # return Response(response=resp_data, status=200, mimetype="text/html")

    return render_template("time.html", current_time= str(datetime.now()))


# Start it on this "PORT"
print("Application will be starting in {} PORT".format(PORT))
app.run(host='0.0.0.0', port=PORT)