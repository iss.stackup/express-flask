//Load require libs
const express = require('express');
const handlebars = require('express-handlebars');
const mysql = require('mysql');

//configure port
const PORT = process.argv[2] || 3000;

//configure mysql
const pool = mysql.createPool({
    host: 'localhost', port: 3306,
    user: 'fred', password: 'fred',
    database: 'sakila',
    connectionLimit: 5
});

//Create an instance of Express
app = express();
app.engine('handlebars', handlebars({ defaultLayout: 'main' }))
app.set('view engine', 'handlebars')

//Define routes
//GET /films?limit=10&offset=100
app.get('/films', (req, resp) => {
    const limit = req.query.limit? parseInt(req.query.limit): 20;
    const offset = req.query.offset? parseInt(req.query.offset): 0;

    pool.getConnection((err, conn) => {
        if (err) {
            console.error('ERROR: ', err);
            resp.status(500).send(err);
            return;
        }
        //Potential SQL injection
        //conn.query('select * from film limit ' +  limit + ' offset ' + offset)
        conn.query('select * from film limit ? offset ?', [ limit, offset ], 
            (err, result) => {
                if (err) {
                    console.error('ERROR: ', err);
                    resp.status(500).send(err);
                    return;
                }
                resp.format({
                    'text/html': () => {
                        resp.render('films', { films: result })
                    },
                    'application/json': () => {
                        resp.status(200).json(result);
                    },
                    'default': () => {
                        resp.status(406)
                        resp.send(`I cannot produce ${req.get('Accept')} `)
                    }
                });
                conn.release();
            }
        )
    })
})


//route GET /time
app.get('/time', (req, resp) => {
    resp.status(200)
    resp.type('text/html')
    resp.render('time', { time: new Date() })
    //resp.send(`<h1>The current time is ${new Date()}</h1>`)
})

//For serving static resources
app.use(express.static(__dirname + '/public'))

//Start the server
app.listen(PORT, () => {
    console.log("Application started at %s on port %d", new Date(), PORT);
})